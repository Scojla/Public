name:Costumbre
weight:10
carriesraid:true

//Vanilla biomes
biome:Swampland
biome:Swampland M
biome:Jungle
biome:JungleEdge
biome:JungleEdge M
biome:Birch Forest
biome:Birch Forest M

//biomes o'plenty
biome:bayou
biome:lush swamp
biome:rainforest
biome:tropical rainforest
biome:wetland

pathmaterial:pathdirt
pathmaterial:pathslabs

//starting buildings:
centre:mayanobservatory
start:mayanhouse
start:mayanhouse
start:mayanlumber
start:mayangrove
start:mayanmining
start:mayanshamangrounds
start:mayaninn

player:mayanplayersmallfield
player:mayanplayersmallhouse
player:mayanplayerlargefield
player:mayanplayerhugefield
player:mayanplayerestate
player:mayanplayerlargehouse
player:gifthouse

//to be built in priority
core:mayanmining
core:mayancornfarm
core:mayangrove
core:mayantemple
core:mayancacaofarm

//to be built after the core ones
secondary:mayanlargepeasanthouse2
secondary:mayanlibrary
secondary:mayanalter
secondary:mayansculptorhouse
secondary:mayancalendar
secondary:mayanmarket
secondary:mayancrafter
secondary:mayanobsidiancrafter

//never for this type of village
never:mayanarmyforge
never:mayanwatchtower
never:mayanguard